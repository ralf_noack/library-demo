package de.ralf.usersapi.usersapi;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.ralf.usersapi.usersapi.model.UserDTO;

@RestController
@RequestMapping("users")
public interface UsersAPI
{

   @GetMapping(value = "/getAll")
   List<UserDTO> getAll();

   @PostMapping(value = "/getById")
   List<UserDTO> getById(List<Long> ids);


   @GetMapping(value = "/getActive")
   List<UserDTO> getActive();

}
