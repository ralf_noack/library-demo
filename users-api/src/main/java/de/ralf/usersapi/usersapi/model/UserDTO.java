package de.ralf.usersapi.usersapi.model;

import java.util.Date;

import lombok.Builder;
import lombok.Getter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Builder
@Jacksonized
public class UserDTO
{
   private Long id;

   private String name;

   private String firstName;

   private Date memberSince;

   private Date memberTill;

   private String gender;
}
