package de.ralf.users.modules;

import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import de.ralf.users.entity.User;
import de.ralf.usersapi.usersapi.model.UserDTO;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class UserToUserDTOFunctionTest
{
   private UserToUserDTOFunction function;
   private User user;

   @BeforeEach
   void init()
   {
      function = new UserToUserDTOFunction();

      user = new User();
      user.setId(1L);
      user.setFirstName("first");
      user.setGender("g");
      user.setName("name");
      user.setMemberSince(new Date(1000L));
      user.setMemberTill(new Date(2000L));
   }

   @Test
   void apply()
   {
      UserDTO dto = function.apply(user);
      assertThat(dto.getFirstName()).isEqualTo("first");
      assertThat(dto.getId()).isEqualTo(1L);
      assertThat(dto.getGender()).isEqualTo("g");
      assertThat(dto.getName()).isEqualTo("name");
      assertThat(dto.getMemberSince()).isEqualTo(new Date(1000L));
      assertThat(dto.getMemberTill()).isEqualTo(new Date(2000L));
   }
}