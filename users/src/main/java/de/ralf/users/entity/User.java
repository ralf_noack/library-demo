package de.ralf.users.entity;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.Hibernate;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class User
{

   @Id
   private Long id;

   private String name;

   private String firstName;

   private Date memberSince;

   private Date memberTill;

   private String gender;

   @Override
   public boolean equals(Object o)
   {
      if (this == o)
      {
         return true;
      }
      if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o))
      {
         return false;
      }
      User user = (User) o;
      return id != null && Objects.equals(id, user.id);
   }

   @Override
   public int hashCode()
   {
      return getClass().hashCode();
   }
}
