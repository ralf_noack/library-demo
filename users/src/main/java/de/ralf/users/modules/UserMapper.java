package de.ralf.users.modules;

import org.mapstruct.factory.Mappers;

import de.ralf.users.entity.User;
import de.ralf.usersapi.usersapi.model.UserDTO;

public interface UserMapper
{
   UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

   UserDTO userToUserDTO(User user);

}
