package de.ralf.users.modules;

import java.util.function.Function;

import org.springframework.stereotype.Component;

import de.ralf.users.entity.User;
import de.ralf.usersapi.usersapi.model.UserDTO;

@Component
public class UserToUserDTOFunction implements Function<User, UserDTO>
{

   @Override
   public UserDTO apply(User user)
   {
      return UserMapper.INSTANCE.userToUserDTO(user);
   }
}
