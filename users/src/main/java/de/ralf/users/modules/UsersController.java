package de.ralf.users.modules;

import java.util.List;

import org.springframework.stereotype.Component;

import de.ralf.users.repository.UserRepository;
import de.ralf.usersapi.usersapi.model.UserDTO;

@Component
public class UsersController
{
   private final UserRepository userRepository;

   public UsersController(UserRepository userRepository)
   {
      this.userRepository = userRepository;
   }

   public List<UserDTO> getAll()
   {
      return null;
   }

   public List<UserDTO> getById(List<Long> ids)
   {
      return null;
   }

   public List<UserDTO> getActive()
   {
      return null;
   }

}
