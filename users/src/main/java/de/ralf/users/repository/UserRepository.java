package de.ralf.users.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import de.ralf.users.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>
{

   List<User> findAllByMemberTillIsNull();
}
