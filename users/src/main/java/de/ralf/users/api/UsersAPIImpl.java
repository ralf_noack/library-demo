package de.ralf.users.api;

import java.util.List;

import org.springframework.stereotype.Component;

import de.ralf.users.modules.UsersController;
import de.ralf.usersapi.usersapi.UsersAPI;
import de.ralf.usersapi.usersapi.model.UserDTO;

@Component
public class UsersAPIImpl implements UsersAPI
{
   private final UsersController usersController;

   public UsersAPIImpl(UsersController usersController)
   {
      this.usersController = usersController;
   }

   @Override
   public List<UserDTO> getAll()
   {
      return null;
   }

   @Override
   public List<UserDTO> getById(List<Long> ids)
   {
      return null;
   }

   @Override
   public List<UserDTO> getActive()
   {
      return null;
   }
}
